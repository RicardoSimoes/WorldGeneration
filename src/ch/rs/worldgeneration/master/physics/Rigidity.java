package ch.rs.worldgeneration.master.physics;

import ch.rs.worldgeneration.master.elements.physicalstates.PhysicalStates;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Rigidity extends Density {

    private double SOLID_RIGIDITY;
    private double LIQUID_RIGIDITY;
    private double rigidity;
    private PhysicalStates state;

    public Rigidity(double mass, double volume, PhysicalStates state) {
        super(mass, volume);
        this.state = state;
        calculatePow();
        calculateRidigity();
    }

    private void calculateRidigity(){
        rigidity = Math.pow(super.getDensity()/100, (Math.PI*getPow()));

    }

    private void calculatePow(){
        SOLID_RIGIDITY = ThreadLocalRandom.current().nextDouble(1.1, 10.0);
        LIQUID_RIGIDITY = ThreadLocalRandom.current().nextDouble(0.1, 1.0);
    }

    private double getPow(){
        switch(state){
            case SOLID:
                return SOLID_RIGIDITY;
            case LIQUID:
                return LIQUID_RIGIDITY;
            default:
                return 0.0;
        }
    }

    public double getRigidity(){
        return rigidity;
    }


}
