package ch.rs.worldgeneration.master.physics;

public class Density {

        private double mass;
        private double density;
        private  double volume;

        public Density(double mass, double volume){
            updateDensity(mass, volume);
        }

        protected void updateDensity(double mass, double volume){
            this.mass = mass;
            this.volume = volume;
            calculateDensity();
        }

        private void calculateDensity(){
            density = mass / volume;
        }

        public double getDensity(){
            return density;
        }
}
