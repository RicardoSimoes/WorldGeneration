package ch.rs.worldgeneration.master.physics;

import ch.rs.worldgeneration.master.elements.physicalstates.PhysicalStates;

public abstract class TemperatureDynamics extends Rigidity {

    private double internalTemperature;

    public TemperatureDynamics(double ambientTemperature, double mass, double volume, PhysicalStates state) {
        super(mass, volume, state);
        internalTemperature = ambientTemperature;
    }

    public void applyTemperatureRadiation(double radiationValue){
        //System.out.println("Old internal temperature is " + internalTemperature);
        internalTemperature +=
                ((radiationValue - internalTemperature)/(Math.PI))/getDensity()/(Math.sqrt(getRigidity()));
        checkForStateChange();
    }

    public double getInternalTemperature(){
        return internalTemperature;
    }

    public abstract void checkForStateChange();


}
