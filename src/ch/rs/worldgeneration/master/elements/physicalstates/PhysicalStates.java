package ch.rs.worldgeneration.master.elements.physicalstates;

public enum PhysicalStates {

    SOLID, LIQUID, GAS, PLASMA;

    @Override
    public String toString(){
        return this.name();
    }

}
