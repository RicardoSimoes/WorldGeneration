package ch.rs.worldgeneration.master.elements.physicalstates;

public class PhysicalStateTemperature {

    private PhysicalStates state;
    private double minTemp;


    public PhysicalStateTemperature(PhysicalStates state, double minTemp){
        this.state = state;
        this.minTemp = minTemp;
    }

    public PhysicalStates getState(){
        return state;
    }

    public double getMinTempNeededForState(){
        return minTemp;
    }

    @Override
    public String toString(){
        return "The state " + state.toString() + " requires a minimal Temperature of " + minTemp;
    }

    public boolean isTemperatureAbove(double actualTemp){
        return (Double.compare(actualTemp, minTemp) > 0);
    }


}
