package ch.rs.worldgeneration.master.elements;


import java.util.concurrent.ThreadLocalRandom;

public class Temperature {

    private double MIN_VALUE;
    private double MAX_VALUE;
    private double MEDIAN_VALUE;

    public Temperature(){
        MIN_VALUE = 0.0;
        generateMaxTemp();
        generateMedianTemp();
    }

    public double getMedianTemp(){
        return MEDIAN_VALUE;
    }

    private void generateMedianTemp(){
        MEDIAN_VALUE = MAX_VALUE / 2.0;
    }

    private void generateMaxTemp(){
        MAX_VALUE = ThreadLocalRandom.current().nextDouble() *
                Math.PI * Math.round(100)*(Math.PI/ThreadLocalRandom.current().nextDouble());

        System.out.println("Max Temperature is " + MAX_VALUE);
    }

    public double getMinTemp(){
        return MIN_VALUE;
    }

    public double getMaxTemp(){
        return MAX_VALUE;
    }


}
