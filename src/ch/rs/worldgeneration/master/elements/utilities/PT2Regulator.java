package ch.rs.worldgeneration.master.elements.utilities;

public class PT2Regulator {

    /**FORMEL:
     *
     * K/((1+T1*s)*(1+T2*s))
     *
     * T1 = Zeit anfang
     * T2 = Zeit Schludd
     *
     * s = input
     *
     * K = verstaerkung
     *
     *
     *
     */

    private double k;
    private double tOne;
    private double tTwo;


    public PT2Regulator(double k, double tOne, double tTwo){
        this.k = k;
        this.tOne = tOne;
        this.tTwo = tTwo;
    }

    public void setK(double k){
        this.k = k;
    }
    public double calculateOutput(double t) {
        if(Double.compare(round(tOne, 1),
                round(tTwo, 1)) == 0){
            return calculateSameDelay(t);
        }
        double firstDelay = calculateFirstDelay(t);
        double secondDelay = calculateSecondDelay(t);
        return k*(1-firstDelay+secondDelay);
    }

    private double calculateSameDelay(double t){
        return k*((1-Math.exp(-(t/tOne)))-(t/tOne)*Math.exp(-(t/tTwo)));
    }

    private double calculateFirstDelay(double t){
        return (tOne*Math.exp(-(t/tOne)))/(tOne-tTwo);
    }

    private double calculateSecondDelay(double t) {
        return (tTwo * Math.exp(-(t / tTwo))) / (tOne - tTwo);
    }

    private double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }
}
