package ch.rs.worldgeneration.master.elements.utilities;

import java.util.Random;

public class ElementNaming {

    private static String[] firstPart = {"act", "alu", "ame", "anti", "cal", "arg", "ber", "hel", "hol", "ir", "xo", "iri", "tri", "don", "xer", "yor"};

    private static String[] middlePart = { "than", "esi", "odi", "tim", "mi", "net", "yer", "ni", "no", "tho", "ron", "forn"};

    private static String[] endPart = {"gen", "um", "un", "on", "om", "us", "ese", "er"};

    public static String generateElementName(){
        Random r = new Random();
        int components = (r.nextInt(8)+1)/2;
        String name = "";
        for(int x = 0; x <= components; x++){
            if(x == 0){
                name += firstPart[r.nextInt(firstPart.length)];
            } else if (x == components){
                name += endPart[r.nextInt(endPart.length)];
            } else {
                name += middlePart[r.nextInt(middlePart.length)];
            }
        }
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
