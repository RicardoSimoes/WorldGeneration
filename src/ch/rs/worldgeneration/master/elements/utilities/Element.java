package ch.rs.worldgeneration.master.elements.utilities;

import java.util.*;

import ch.rs.worldgeneration.master.elements.physicalstates.*;
import ch.rs.worldgeneration.master.physics.TemperatureDynamics;
import javafx.util.Pair;

public class Element extends TemperatureDynamics{

    private String elementName = "";
    private PhysicalStates currentState;
    private List<Pair<PhysicalStates, Double>> statesList = new LinkedList();

    public Element(double ambientTemperature, double mass, double volume, LinkedList<Pair<PhysicalStates, Double>> statesList, String name) {
        super(ambientTemperature, mass, volume, PhysicalStates.SOLID);
        this.elementName = name;
        this.statesList = statesList;
        System.out.println("Generated Element " + elementName + " with the following stats:");
        for(Pair<PhysicalStates, Double> pair : statesList){
            System.out.println(pair.getKey().toString() + " with the minimal Temp of " + pair.getValue());
        }
        System.out.println("Rigidity of " + super.getRigidity());
        System.out.println("Density of " + super.getDensity());
        System.out.println("=========================================================");
    }


    private void setState(PhysicalStates state){
        if(state != null) {
            if(currentState != state) {
                //System.out.println(elementName + " new state of " + state.toString());
                this.currentState = state;
            }
        }
    }

    public PhysicalStates getElementState(){
        return currentState;
    }


    @Override
    public void checkForStateChange() {
        PhysicalStates state = null;
        for(Pair<PhysicalStates, Double> pair : statesList){
            if(Double.compare(super.getInternalTemperature(), pair.getValue()) > 0){
                state = pair.getKey();
            }
        }
        if(state == null){
            state = statesList.get(0).getKey();
        }
        setState(state);
    }

    public String getName(){
        return elementName;
    }

    @Override
    public String toString(){/**
        return "The Element " + elementName + " has the current State of " + currentState.toString()
                + ". It has an internal Temperature of " + super.getInternalTemperature() + ", a density of " +
                super.getDensity() + " aswell as a rigidity of " + super.getRigidity() + ".";**/
    return super.getInternalTemperature() +";";
    }
}
