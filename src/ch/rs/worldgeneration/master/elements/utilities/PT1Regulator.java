package ch.rs.worldgeneration.master.elements.utilities;

public class PT1Regulator {

    /**
     * K = value to reach
     * t = delay
     */

    private double k;
    private double t = 5.0;

    public PT1Regulator(double k){
        this.k = k;
    }

    /**
     * Give this function the difference between is and should be
     * @param s the difference between is and should be
     * @return value to add to already existing value
     */
    public double calculateOutput(double s){
        return k*(1-Math.exp(-(s/t)));
    }
}
