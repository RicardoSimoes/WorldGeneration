package ch.rs.worldgeneration.master;

import ch.rs.worldgeneration.master.elements.Temperature;
import ch.rs.worldgeneration.master.elements.physicalstates.PhysicalStates;
import ch.rs.worldgeneration.master.elements.utilities.Element;
import ch.rs.worldgeneration.master.elements.utilities.ElementNaming;
import ch.rs.worldgeneration.master.elements.utilities.PT2Regulator;
import ch.rs.worldgeneration.master.world.World;
import javafx.util.Pair;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    static Temperature worldTemperature;
    static long physicsTime= 1000/60;

    public static void main(String[] args) throws Exception{
        PrintWriter out = new PrintWriter("ElementTemperatureData.csv");
        worldTemperature = new Temperature();
        LinkedList<Element> elements = new LinkedList();
        System.out.println("How many elements do you want to generate?");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        System.out.println("Generating " + x + " elements...");
        for(int l = 0; l < x; l++){
            elements.add(generateElement());
        }
        World world = new World(worldTemperature.getMedianTemp(), "newWorld");
        System.out.println("New world called " + world.getName() + " with the median temp of " + world.getTemperatureAtEquator());
        System.out.println("Maximal Temperature is " + worldTemperature.getMaxTemp());
        System.out.println("Set Ambient Temperature to: ");
        int temp = scanner.nextInt();
        System.out.println("How long do you want the simulation to run?");
        int calculations = scanner.nextInt();
       /* for(int u = 1; u <= calculations; u++){
            System.out.println("Phyis Calc " + u);
            if(u == 1){
                for(Element e : elements) {
                    out.print(e.getName() + ";");
                }
                out.println();
            }
            for(Element e : elements){
                //System.out.println("Calculating " + u + "...");
                e.applyTemperatureRadiation(temp);
                out.print(e.toString());
                //System.out.println("====================================");
            }
            out.println();
            Thread.sleep(physicsTime);

        }*/

        PT2Regulator gliedOne = new PT2Regulator(50, 125.1, 125.0);
        double toReach = 200.0;
        double is = 150.0;
        double tempValue = 0.0;
        double secElapsed = 0.0;
        System.out.println(is+";");
        boolean notDone = true;
        boolean doneTwo = true;
        gliedOne.setK(toReach-is);
        while(is + tempValue < toReach){
            tempValue = gliedOne.calculateOutput(secElapsed);
            out.println(tempValue + is +";");
            secElapsed++;
            if(is+tempValue < 100.0 &&!notDone && doneTwo){
                System.out.println("set K to 50");
                gliedOne.setK(toReach);
                secElapsed = 0.0;
                is += tempValue;
                tempValue = 0.0;
                doneTwo = false;
            }
            if(is+tempValue > 180.0 && notDone){
                System.out.println("set K to -125");
                notDone = false;
                is += tempValue;
                tempValue = 0.0;
                gliedOne.setK(-125);
                secElapsed = 0.0;
            }
            System.out.println(tempValue);
        }

        out.close();

    }

    public static  Element generateElement(){
        LinkedList<Pair<PhysicalStates, Double>> states = new LinkedList();
        double newTemp;
        for(PhysicalStates state : PhysicalStates.values()){
            if(states.isEmpty()){
                states.add(new Pair<PhysicalStates, Double>(state, 0.0));
            } else {
                newTemp = randomTemperatureValue();
                while(newTemp < states.getLast().getValue()){
                    newTemp = randomTemperatureValue();
                }
                states.add(new Pair<PhysicalStates, Double>(state, newTemp));
            }

        }
        return new Element(worldTemperature.getMedianTemp(), randomMassValue(), 1.0, states, ElementNaming.generateElementName());
    }

    public static double randomTemperatureValue(){
        return ThreadLocalRandom.current().nextDouble(worldTemperature.getMinTemp(),
                worldTemperature.getMaxTemp());
    }

    public static double randomMassValue(){
        return (ThreadLocalRandom.current().nextDouble(1, Math.PI))*100;
    }
}
