package ch.rs.worldgeneration.master.world;

public class World {

    private double temperatureAtEquator;
    private String worldName;

    public World(double temperatureAtEquator, String worldName){
        this.temperatureAtEquator = temperatureAtEquator;
        this.worldName = worldName;
    }

    public String getName(){
        return worldName;
    }

    public double getTemperatureAtEquator(){
        return temperatureAtEquator;
    }


    @Override
    public String toString(){
        return worldName;
    }

}
